﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class Index : System.Web.UI.Page
    {
        services objServ = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            // var objService = new ServiceReference1();

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            

            string result = objServ.Login(txtUserName.Text, txtPassword.Text);
            if (!String.IsNullOrEmpty(result))
            {
                Session["UserID"] = result;
                Response.Redirect("Home.aspx");
            }
            else
            {
                Response.Write("<script>alert('Please Enter valid username and password...')</script>");
            }
            
        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            User objUser = new User();
            objUser.FirstName = TextBox1.Text.Trim();
            objUser.LastName = TextBox2.Text.Trim();
            objUser.Email = TextBox3.Text.Trim();
            objUser.UserName = TextBox4.Text.Trim();
            objUser.Password = TextBox5.Text.Trim();

            string _result = objServ.CreateUser(objUser);

            if (!String.IsNullOrEmpty(_result))
            {
                Response.Write("<script>alert('This user successfully created...')</script>");
            }
            else
            {
                Response.Write("<script>alert('Please Enter valid username and password...')</script>");
            }

        }
    }
}