﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class IncomeCategory1 : System.Web.UI.Page
    {
        services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                get_list(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }

        public void get_list(string UserID)
        {
            List<IncomeCategory> _listCat = _objSer.get_InCategory(UserID);
            List<custom_incategory> _listCusCat = new List<custom_incategory>();
            int i = 0;
            foreach (IncomeCategory _objInCt in _listCat)
            {
                i++;
                custom_incategory _objInCat = new custom_incategory()
                {
                    SlNo = i,
                    ID = _objInCt.ID,
                    Name = _objInCt.Name,
                    Status = _objInCt.Status == 1 ? "Active" : "Inactive"
                };
                _listCusCat.Add(_objInCat);
            }
            rptCategory.DataSource = _listCusCat;
            rptCategory.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            IncomeCategory objinCat = new IncomeCategory()
            {
                Name = txtInputName.Text,
                UserID = Convert.ToInt32(Session["UserID"].ToString())
            };
            string result = _objSer.Create_IncomeCategory(objinCat);

            Response.Write("<script>alert('" + result + "')</script>");
            get_list(Session["UserID"].ToString());
            txtInputName.Text = "";
        }

    }

        public class custom_incategory
        {
            public int SlNo { get; set; }

            public int ID { get; set; }

            public string Name { get; set; }

            public string Status { get; set; }
        }
  

}
