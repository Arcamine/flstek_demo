﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Savings.aspx.cs" Inherits="WebServices.Savings1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-panel">        
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">New Savings</h4>
                         
                           
                            <div class="form-group">
                              <label for="exampleInputPassword4">Date</label>
                                <div id="datepicker-popup" class="input-group date datepicker">
                                    <input type="text" class="form-control" runat="server" id="txtDate" autocomplete="off">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="ti-calendar input-group-text"></span>
                                        </span>
                                </div>
                            </div>

                             <div class="form-group">
                              <label for="exampleInputEmail3">Amount</label>
                                <asp:TextBox ID="txtInputAmt" CssClass="form-control" runat="server" placeholder="Amount" autocomplete="off"></asp:TextBox>
                            </div>
                              <asp:Button runat="server" CssClass="btn btn-primary mr-2" Text="Submit" ID="btnSubmit" OnClick="btnSubmit_Click" />
                            <button class="btn btn-light">Cancel</button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
    <script>
        $(document).ready(function () {
            $('#datepicker-popup').datepicker();
        });

    </script>
</asp:Content>
