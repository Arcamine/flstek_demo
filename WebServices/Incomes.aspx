﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Incomes.aspx.cs" Inherits="WebServices.Incomes1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-panel">        
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">New Incomes</h4>
                            <div class="form-group">
                              <label for="exampleInputName1">Income Type</label>
                                <asp:DropDownList runat="server" ID="ddlCat" ClientIDMode="Static" CssClass="form-control" DataTextField = "Name" DataValueField = "ID" AutoPostBack="true">
                                    </asp:DropDownList>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail3">Income Amount</label>
                                <asp:TextBox ID="txtInputAmt" CssClass="form-control" runat="server" placeholder="Amount"></asp:TextBox>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword4">Receive Date</label>
                                <div id="datepicker-popup" class="input-group date datepicker">
                                    <input type="text" class="form-control" runat="server" id="txtDate" autocomplete="off">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="ti-calendar input-group-text"></span>
                                        </span>
                                </div>
                            </div>
                              <asp:Button runat="server" CssClass="btn btn-primary mr-2" Text="Submit" ID="btnSubmit" OnClick="btnSubmit_Click" />
                            <button class="btn btn-light">Cancel</button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
    <script>
        $(document).ready(function () {
            $('#datepicker-popup').datepicker();
        });
        
    </script>
</asp:Content>
