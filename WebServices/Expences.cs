﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServices
{
    public class Chart1
    {
        public string Months { get; set; }

        public double Total_ex { get; set; }

        public double Total_In { get; set; }
    }
    public class Expences
    {
        public int CatID { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public int UserID { get; set; }

    }

    public class Incomes
    {
        public int CatID { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public int UserID { get; set; }

    }

    public class Savings
    {
        public int UserID { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    
    }

    public class Expence_View
    {
        public string CatName { get; set; }
        public decimal Amount { get; set; }
        public string Date { get; set; }
    }

    public class Income_View
    {
        public string CatName { get; set; }
        public decimal Amount { get; set; }
        public string Date { get; set; }
    }

    public class Savings_View
    {
        public decimal Amount { get; set; }
        public string Date { get; set; }
    }

    public class ExpenseCategory
    {
        public int UserID { get; set; }

        public int ID { get; set; }
        public string Name { get; set; }

        public int Status { get; set; }
    }
    public class IncomeCategory
    {
        public int UserID { get; set; }

        public int ID { get; set; }
        public string Name { get; set; }

        public int Status { get; set; }
    }
    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

    }
}