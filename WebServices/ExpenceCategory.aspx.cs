﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                get_list(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }

        public void get_list( string UserID)
        {
            List<ExpenseCategory> _listCat = _objSer.get_ExCategory(UserID);
            List<custom_category> _listCusCat = new List<custom_category>();
            int i = 0;
            foreach (ExpenseCategory _objExCt in _listCat)
            {
                i++;
                custom_category _objCuCt = new custom_category()
                {
                    SlNo = i,
                    ID = _objExCt.ID,
                    Name = _objExCt.Name,
                    Status = _objExCt.Status == 1 ? "Active" : "Inactive"
                };
                _listCusCat.Add(_objCuCt);
            }
            rptCategory.DataSource = _listCusCat;
            rptCategory.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ExpenseCategory objexCat = new ExpenseCategory()
            {
                Name = txtInputName.Text,
                UserID = Convert.ToInt32(Session["UserID"].ToString())
            };
            string result = _objSer.Create_ExpenceCategory(objexCat);

            Response.Write("<script>alert('" + result + "')</script>");
            get_list(Session["UserID"].ToString());
            txtInputName.Text = "";
        }
    }

    public class custom_category
    {
        public int SlNo { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }
    }
}