﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebServices
{
    public partial class ViewIncomes : System.Web.UI.Page
    {

        services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                get_Income(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("Index.aspx");

            }
        }

        public void get_Income(string UserID)
        {
            List<Income_View> _listCat = _objSer.get_Income(UserID);
            List<custom_incomeView> _listCusCat = new List<custom_incomeView>();
            int i = 0;
            foreach (Income_View _objInView in _listCat)
            {
                i++;
                custom_incomeView _objCuView = new custom_incomeView()
                {
                    SlNo = i,
                    CatName = _objInView.CatName,
                    Amount = _objInView.Amount,
                    Date = _objInView.Date
                };
                _listCusCat.Add(_objCuView);
            }
            rptIncomes.DataSource = _listCusCat;
            rptIncomes.DataBind();
        }

        public class custom_incomeView
        {
            public int SlNo { get; set; }
            public string CatName { get; set; }

            public decimal Amount { get; set; }
            public string Date { get; set; }
        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            string[] arr_date = txtDateFrom.Value.Split('/');
            string dateUp1 = arr_date[1] + "-" + arr_date[0] + "-" + arr_date[2];
            string date1 = Convert.ToDateTime(dateUp1).ToString("MM-dd-yyyy");
            string[] arr_date1 = txtDateTo.Value.Split('/');
            string dateUp2 = arr_date1[1] + "-" + arr_date1[0] + "-" + arr_date1[2];
            string date2 = Convert.ToDateTime(dateUp2).ToString("MM-dd-yyyy");
            List<Income_View> obj_inList = _objSer.SearchIncome(date1, date2, Session["UserID"].ToString());
            List<custom_incomeView> _listCusCat = new List<custom_incomeView>();
            int i = 0;
            foreach (Income_View _objInView in obj_inList)
            {
                i++;
                custom_incomeView _objCuView = new custom_incomeView()
                {
                    SlNo = i,
                    CatName = _objInView.CatName,
                    Amount = _objInView.Amount,
                    Date = _objInView.Date
                };
                _listCusCat.Add(_objCuView);
            }
            rptIncomes.DataSource = _listCusCat;
            rptIncomes.DataBind();
        }
    }
}
