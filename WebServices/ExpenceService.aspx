﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpenceService.aspx.cs" Inherits="WebServices.ExpenceService" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div  style="margin: 7% 25% 0% 25%; border-inline-color: inherit; border: solid #439091; background-color: #c5fdf7;padding: 13px;">
            <div style="text-align:center;margin-bottom: 12px;">
                <div style="margin-right: 80px;">
                    <span> Enter Category</span>
                </div>
                <div>
                    <span>
                        <asp:TextBox ID="txtCategory" runat="server" ClientIDMode="Static"></asp:TextBox>
                    </span>
                </div>
            </div>
            
            <div style="text-align:center;margin-bottom: 17px;">
                    <asp:Button ID="btnSave" runat="server" ClientIDMode="Static" OnClick="btnSave_Click" Text="Save" />
            </div>
        </div>
        
        <div  style="margin: 7% 25% 0% 25%; border-inline-color: inherit; border: solid #439091; background-color: #c5fdf7;">

            <asp:GridView ID="GridView1" runat="server">
            </asp:GridView>

        </div>
    </form>
</body>
</html>
