﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Data;
using WebServices.localhost;

namespace WebServices
{
    public partial class ExpenceService : System.Web.UI.Page
    {
        static SqlConnection _scon = new SqlConnection(ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString);
        services objser = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                getList();
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ExpenseCategory objexCat = new ExpenseCategory()
            {
                Name = txtCategory.Text,
                UserID = Convert.ToInt32(Session["UserID"].ToString())
            };
            string result = objser.Create_ExpenceCategory(objexCat);

            Response.Write("<script>alert('" + result + "')</script>");
            getList();
            txtCategory.Text = "";
        }



        public void getList()
        {
            GridView1.DataSource = objser.get_ExCategory(Session["UserID"].ToString());
            GridView1.DataBind();
        }
    }
}