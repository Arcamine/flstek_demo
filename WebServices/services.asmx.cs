﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebServices
{
    /// <summary>
    /// Summary description for services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")] // for uniquely identifying our service on the internet from other services that are already there on the web, we use Webservice Nameespace
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class services : System.Web.Services.WebService
    {
        SqlConnection _scon = new SqlConnection(ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString);

        [WebMethod]

        public string Login(string username, string password)
        {
            string user_id = "";
            try
            {
                string _userLogin = "select * from tbl_User where UserName='" + username + "' and Password='" + password + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_userLogin, _scon);
                DataTable _dt = new DataTable(); //collection of columns and rows to store data in a grid form
                SqlDataReader _dr = _com.ExecuteReader(); //return the set of rows, on execution of SQL Query or Stored procedure using command object
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    user_id = _dt.Rows[i]["ID"].ToString(); ;
                }
            }
            catch(Exception ex)
            {
                user_id = "";
            }
            return user_id;
        }

        [WebMethod]
        
        public string Create_ExpenceCategory(ExpenseCategory ExCat)
        {
            int insert_type = 0;
            string result = "";
            try
            {
                string _sqlExCat = "INSERT INTO ExpenseCategory_tbl (Name,UserID) VALUES ('" + ExCat.Name + "','" + ExCat.UserID + "')";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlExCat, _scon);
                insert_type = _com.ExecuteNonQuery();
                _scon.Close();
                if (insert_type == 1)
                {
                    result = "Success";
                }
                else
                {
                    result = "Failed";
                }
            }
            catch(Exception ex)
            {
                result = "Failed";
            }
            return result;
        }


        [WebMethod]
        public List<ExpenseCategory> get_ExCategory(string UserID)
        {
            List<ExpenseCategory> objExCate = new List<ExpenseCategory>();
            try
            {
                string _sqlExpences = "SELECT * FROM ExpenseCategory_tbl WHERE UserID='"+ UserID + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlExpences, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    ExpenseCategory _objex = new ExpenseCategory();
                    _objex.Name = _dt.Rows[i]["Name"].ToString();
                    _objex.UserID = Convert.ToInt32(_dt.Rows[i]["UserID"].ToString());
                    _objex.ID = Convert.ToInt32(_dt.Rows[i]["ID"].ToString());
                    _objex.Status = Convert.ToInt32(_dt.Rows[i]["Status"].ToString());
                    objExCate.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objExCate = null;
            }
            return objExCate;
        }
        
        [WebMethod]

        public string CreateUser(User objUser)
        {
            int _queryResult = 0;
            string _Result = "";
            try
            {
                string _sqlUser = "INSERT INTO tbl_User (FirstName,LastName,Email,UserName,Password) " +
                    "VALUES ('" + objUser.FirstName + "','" + objUser.LastName + "','" + objUser.Email + "','" + objUser.UserName + "','" + objUser.Password + "')";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlUser, _scon);
                _queryResult = _com.ExecuteNonQuery();
                _scon.Close();
                if (_queryResult == 1)
                {
                    _Result = "Success";
                }
                else
                {
                    _Result = "Failed";
                }
            }
            catch (Exception ex)
            {
                _Result = "Failed";
            }
            return _Result;
        }

        [WebMethod]

        public List<Expence_View> SearchExpence(string dateFrom , string dateTo, string UserID)
        {
            List<Expence_View> objlistExpence = new List<Expence_View>();
            try
            {
                string sqlExpences = "SELECT ex.Amount,ex.Date,ec.Name FROM Expense_tbl ex join ExpenseCategory_tbl ec on ex.CatID = ec.ID " +
                    "WHERE ex.UserId='" + UserID + "' and Date between '" + dateFrom + "' and '" + dateTo + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(sqlExpences, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    Expence_View _objex = new Expence_View();

                    _objex.CatName = _dt.Rows[i]["Name"].ToString();
                    _objex.Amount = Decimal.Parse(_dt.Rows[i]["Amount"].ToString());
                    _objex.Date = Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");

                    objlistExpence.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objlistExpence = null;
            }
            return objlistExpence;
        }


        [WebMethod]

        public List<Income_View> SearchIncome(string dateFrom, string dateTo, string UserID)
        {
            List<Income_View> objlistIncome = new List<Income_View>();
            try
            {
                string sqlIncomes = "SELECT ex.Amount,ex.Date,ec.Name FROM  Income_tbl ex join IncomeCategory_tbl ec on ex.CatID = ec.ID " +
                    "WHERE ex.UserId='" + UserID + "' and Date between '" + dateFrom + "' and '" + dateTo + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(sqlIncomes, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    Income_View _objex = new Income_View();

                    _objex.CatName = _dt.Rows[i]["Name"].ToString();
                    _objex.Amount = Decimal.Parse(_dt.Rows[i]["Amount"].ToString());
                    _objex.Date = Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");

                    objlistIncome.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objlistIncome = null;
            }
            return objlistIncome;
        }


        [WebMethod]

        public List<Savings_View> SearchSavings(string dateFrom, string dateTo, string UserID)
        {
            List<Savings_View> objlistSavings = new List<Savings_View>();
            try
            {
                string sqlSavings = "SELECT * FROM Savings_tbl WHERE UserID='" + UserID + "' and Date between '" + 
                    Convert.ToDateTime(dateFrom) + "' and '" + Convert.ToDateTime(dateTo) + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(sqlSavings, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    Savings_View _objex = new Savings_View();

                    _objex.Amount = Decimal.Parse(_dt.Rows[i]["Amount"].ToString());
                    _objex.Date = Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");

                    objlistSavings.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objlistSavings = null;
            }
            return objlistSavings;
        }


        [WebMethod]
        public string CreateExpence(Expences expences)
        {
            int _queryResult = 0;
            string _Result = "";
            try
            {
                string _sqlExpences = "INSERT INTO Expense_tbl (CatID,UserID,Amount,Date) " +
                    "VALUES ('" + expences.CatID + "','" + expences.UserID + "','" + expences.Amount + "','" + expences.Date + "')";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlExpences, _scon);
                _queryResult = _com.ExecuteNonQuery();
                _scon.Close();
                if (_queryResult == 1)
                {
                    _Result = "Success";
                }
                else
                {
                    _Result = "Failed";
                }
            }
            catch (Exception ex)
            {
                _Result = "Failed";
            }
            return _Result;
        }

         [WebMethod]
         public string get_Firstname(int UserID)
        {
            string firstname = "";
            try
            {
                string _userLogin = "select * from tbl_User where ID='" + UserID + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_userLogin, _scon);
                DataTable _dt = new DataTable(); //collection of columns and rows to store data in a grid form
                SqlDataReader _dr = _com.ExecuteReader(); //return the set of rows, on execution of SQL Query or Stored procedure using command object
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    firstname = _dt.Rows[i]["FirstName"].ToString(); ;
                }
            }
            catch (Exception ex)
            {
                firstname = "";
            }
            return firstname;
        }

        [WebMethod]
        public List<Expence_View> get_Expence(string UserID)
        {
            List<Expence_View> objExpences = new List<Expence_View>();
            try
            {
                string _sqlExpences = "SELECT ex.Amount,ex.Date,ec.Name FROM Expense_tbl ex join ExpenseCategory_tbl ec on ex.CatID = ec.ID WHERE ex.UserId='" + UserID + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlExpences, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    Expence_View _objex = new Expence_View();
                    
                    _objex.CatName = _dt.Rows[i]["Name"].ToString();
                    _objex.Amount = Decimal.Parse(_dt.Rows[i]["Amount"].ToString());
                    _objex.Date = Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");

                    objExpences.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objExpences = null;
            }
            return objExpences;
        }

        [WebMethod]

        public List<Chart1> get_expense_r_income_monthly(string type)
        {
            List<Chart1> _objChart = new List<Chart1>();
            if (type == "Expense")
            {
                try
                {
                    string _sqlExpences = "SELECT CASE WHEN MONTH(Date)=1 THEN 'Jan' WHEN MONTH(Date)=2 THEN 'Feb' WHEN MONTH(Date)=3 THEN 'Mar' WHEN MONTH(Date)= 4 THEN 'Apr' " +
                        "WHEN MONTH(Date)= 5 THEN 'May' WHEN MONTH(Date)= 6 THEN 'Jun' WHEN MONTH(Date)= 7 THEN 'Jul' WHEN MONTH(Date)= 8 THEN 'Aug' WHEN MONTH(Date)= 9 THEN 'Sep' " +
                        "WHEN MONTH(Date)= 10 THEN 'Oct' WHEN MONTH(Date)= 11 THEN 'Nov' WHEN MONTH(Date)= 12 THEN 'DEC' END as Monthly, SUM([Amount]) AS TotalExpense FROM[Expense_tbl] WHERE YEAR(Date)=YEAR(GETDATE())" +
                        "GROUP BY MONTH(Date) ORDER BY MONTH(Date)";
                    _scon.Open();
                    SqlCommand _com = new SqlCommand(_sqlExpences, _scon);
                    DataTable _dt = new DataTable();
                    SqlDataReader _dr = _com.ExecuteReader();
                    _dt.Load(_dr);
                    for (int i = 0; i < _dt.Rows.Count; i++)
                    {
                        string _query = "SELECT SUM([Amount]) as TotalIncome from Income_tbl where FORMAT(Date, 'MMM', 'en-US')='" + _dt.Rows[i]["Monthly"].ToString() + "'";
                        SqlCommand _com1 = new SqlCommand(_query, _scon);
                        DataTable _dt1 = new DataTable();
                        SqlDataReader _dr1 = _com1.ExecuteReader();
                        _dt1.Load(_dr1);
                        double totalIncome = 0;
                        if (!String.IsNullOrEmpty(_dt1.Rows[0]["TotalIncome"].ToString()))
                        {
                            totalIncome= Double.Parse(_dt1.Rows[0]["TotalIncome"].ToString());
                        }
                        Chart1 _objex = new Chart1();

                        _objex.Months = _dt.Rows[i]["Monthly"].ToString();
                        _objex.Total_ex = Double.Parse(_dt.Rows[i]["TotalExpense"].ToString());
                        _objex.Total_In = totalIncome;
                        _objChart.Add(_objex);
                    }
                    _scon.Close();
                }
                catch (Exception ex)
                {
                    _objChart = null;
                }
            }
            return _objChart;
        }

        //Income Functions

        [WebMethod]
        public string CreateIncome(Incomes incomes)
        {
            int _queryResult = 0;
            string _Result = "";
            try
            {
                string _sqlIncomes = "INSERT INTO Income_tbl (CatID,UserID,Amount,Date) " +
                    "VALUES ('" + incomes.CatID + "','" + incomes.UserID + "','" + incomes.Amount + "','" + incomes.Date + "')";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlIncomes, _scon);
                _queryResult = _com.ExecuteNonQuery();
                _scon.Close();
                if (_queryResult == 1)
                {
                    _Result = "Success";
                }
                else
                {
                    _Result = "Failed";
                }
            }
            catch (Exception ex)
            {
                _Result = "Failed";
            }
            return _Result;

        }

        [WebMethod]
        public List<Income_View> get_Income(string UserID)
        {
            List<Income_View> objIncomes = new List<Income_View>();
            try
            {
                string _sqlIncomes = "SELECT ex.Amount,ex.Date,ec.Name FROM Income_tbl ex join IncomeCategory_tbl ec on ex.CatID = ec.ID WHERE ex.UserId='" + UserID + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlIncomes, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    Income_View _objex = new Income_View();

                    _objex.CatName = _dt.Rows[i]["Name"].ToString();
                    _objex.Amount = Decimal.Parse(_dt.Rows[i]["Amount"].ToString());
                    _objex.Date = Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");

                    objIncomes.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objIncomes = null;
            }
            return objIncomes;
        }

        [WebMethod]
        public string Create_IncomeCategory(IncomeCategory InCat)
        {
            int insert_type = 0;
            string result = "";
            try
            {
                string _sqlInCat = "INSERT INTO IncomeCategory_tbl (Name,UserID) VALUES ('" + InCat.Name + "','" + InCat.UserID + "')";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlInCat, _scon);
                insert_type = _com.ExecuteNonQuery();
                _scon.Close();
                if (insert_type == 1)
                {
                    result = "Success";
                }
                else
                {
                    result = "Failed";
                }
            }
            catch (Exception ex)
            {
                result = "Failed";
            }
            return result;
        }

 

        [WebMethod]
        public List<IncomeCategory> get_InCategory(string UserID)
        {
            List<IncomeCategory> objInCat = new List<IncomeCategory>();
            try
            {
                string _sqlIncomes = "SELECT * FROM IncomeCategory_tbl WHERE UserID='" + UserID + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlIncomes, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    IncomeCategory _objex = new IncomeCategory();
                    _objex.Name = _dt.Rows[i]["Name"].ToString();
                    _objex.UserID = Convert.ToInt32(_dt.Rows[i]["UserID"].ToString());
                    _objex.ID = Convert.ToInt32(_dt.Rows[i]["ID"].ToString());
                    _objex.Status = Convert.ToInt32(_dt.Rows[i]["Status"].ToString());
                    objInCat.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objInCat = null;
            }
            return objInCat;
        }

        [WebMethod]
        public string CreateSavings(Savings savings)
        {
            int _queryResult = 0;
            string _Result = "";
            try
            {
                string _sqlIncomes = "INSERT INTO Savings_tbl (UserID,Amount,Date) " +
                    "VALUES ('" + savings.UserID + "','" + savings.Amount + "','" + savings.Date + "')";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlIncomes, _scon);
                _queryResult = _com.ExecuteNonQuery();
                _scon.Close();
                if (_queryResult == 1)
                {
                    _Result = "Success";
                }
                else
                {
                    _Result = "Failed";
                }
            }
            catch (Exception ex)
            {
                _Result = "Failed";
            }
            return _Result;
         }

        public List<Savings_View> get_Savings(string UserID)
        {
            List<Savings_View> objSavings = new List<Savings_View>();
            try
            {
                string _sqlIncomes = "SELECT * FROM Savings_tbl WHERE UserID='" + UserID + "'";
                _scon.Open();
                SqlCommand _com = new SqlCommand(_sqlIncomes, _scon);
                DataTable _dt = new DataTable();
                SqlDataReader _dr = _com.ExecuteReader();
                _dt.Load(_dr);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    Savings_View _objex = new Savings_View();
                   
                    _objex.Amount = Decimal.Parse(_dt.Rows[i]["Amount"].ToString());
                    _objex.Date = Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");
                    //Convert.ToDateTime(_dt.Rows[i]["Date"].ToString()).ToString("MM-dd-yyyy");

                    objSavings.Add(_objex);
                }
                _scon.Close();
            }
            catch (Exception ex)
            {
                objSavings = null;
            }
            return objSavings;
        }
    }
}
