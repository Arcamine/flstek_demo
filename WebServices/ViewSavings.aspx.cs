﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class ViewSavings : System.Web.UI.Page
    {
         services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserID"] != null)
            {
                get_Savings(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }

    public void get_Savings(string UserID)
    {
        List<Savings_View> _listCat = _objSer.get_Savings(UserID);
        List<custom_savingsView> _listCusCat = new List<custom_savingsView>();
        int i = 0;
        foreach (Savings_View _objSaView in _listCat)
        {
            i++;
            custom_savingsView _objCuView = new custom_savingsView()
            {
                SlNo = i,
                Amount = _objSaView.Amount,
                Date = _objSaView.Date
            };
            _listCusCat.Add(_objCuView);
        }
        rptSavings.DataSource = _listCusCat;
        rptSavings.DataBind();
    }

    public class custom_savingsView
    {
        public int SlNo { get; set; }

        public decimal Amount { get; set; }
        public string Date { get; set; }
    }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            string[] arr_date = txtDateFrom.Value.Split('/');
            string dateUp1 = arr_date[1] + "-" + arr_date[0] + "-" + arr_date[2];
            string date1 = Convert.ToDateTime(dateUp1).ToString("MM-dd-yyyy");
            string[] arr_date1 = txtDateTo.Value.Split('/');
            string dateUp2 = arr_date1[1] + "-" + arr_date1[0] + "-" + arr_date1[2];
            string date2 = Convert.ToDateTime(dateUp2).ToString("MM-dd-yyyy");
            List<Savings_View> obj_saList = _objSer.SearchSavings(date1, date2, Session["UserID"].ToString());
            List<custom_savingsView> _listCusCat = new List<custom_savingsView>();
            int i = 0;
            foreach (Savings_View _objSaView in obj_saList)
            {
                i++;
                custom_savingsView _objCuView = new custom_savingsView()
                {
                    SlNo = i,
                    Amount = _objSaView.Amount,
                    Date = _objSaView.Date
                };
                _listCusCat.Add(_objCuView);
            }
            rptSavings.DataSource = _listCusCat;
            rptSavings.DataBind();
        }

    }
    }