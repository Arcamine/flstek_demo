﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class Savings1 : System.Web.UI.Page
    {
        services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {

               //_objSer.get_Savings(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("Index.aspx");

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string[] arr_date = txtDate.Value.Split('/');
            string date_new = arr_date[1] + "-" + arr_date[0] + "-" + arr_date[2];
            Savings _objSavings = new Savings()
            {
                Amount = Convert.ToDecimal(txtInputAmt.Text),
                Date = Convert.ToDateTime(date_new),
                UserID=Convert.ToInt32(Session["UserID"].ToString())
            };

            string _result = _objSer.CreateSavings(_objSavings);
            Response.Write("<script>alert('" + _result + "')</script>");
            txtInputAmt.Text = "";
            txtDate.Value = "";
        }
    }
}