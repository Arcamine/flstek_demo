﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class ViewExpenses : System.Web.UI.Page
    {
        services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                get_Expence(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("Index.aspx");

            }

        }

        protected void btnSubmit_Click(object sender,EventArgs e)
        {

        }

        public void get_Expence(string UserID)
        {
            List<Expence_View> _listCat = _objSer.get_Expence(UserID);
            List<custom_expenceView> _listCusCat = new List<custom_expenceView>();
            int i = 0;
            foreach (Expence_View _objExView in _listCat)
            {
                i++;
                custom_expenceView _objCuView = new custom_expenceView()
                {
                    SlNo = i,
                    CatName = _objExView.CatName,
                    Amount = _objExView.Amount,
                    Date = _objExView.Date
                };
                _listCusCat.Add(_objCuView);
            }
            rptExpences.DataSource = _listCusCat;
            rptExpences.DataBind();
        }

        public class custom_expenceView
        {
            public int SlNo { get; set; }
            public string CatName { get; set; }

            public decimal Amount { get; set; }
            public string Date { get; set; }
        }

        //Date Search
        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            string[] arr_date = txtDateFrom.Value.Split('/');
            string dateUp1 = arr_date[1] + "-" + arr_date[0] + "-" + arr_date[2];
            string date1 = Convert.ToDateTime(dateUp1).ToString("MM-dd-yyyy");
            string[] arr_date1 = txtDateTo.Value.Split('/');
            string dateUp2 = arr_date1[1] + "-" + arr_date1[0] + "-" + arr_date1[2];
            string date2 = Convert.ToDateTime(dateUp2).ToString("MM-dd-yyyy");
            List<Expence_View> obj_exList = _objSer.SearchExpence(date1, date2, Session["UserID"].ToString());
            List<custom_expenceView> _listCusCat = new List<custom_expenceView>();
            int i = 0;
            foreach (Expence_View _objExView in obj_exList)
            {
                i++;
                custom_expenceView _objCuView = new custom_expenceView()
                {
                    SlNo = i,
                    CatName = _objExView.CatName,
                    Amount = _objExView.Amount,
                    Date = _objExView.Date
                };
                _listCusCat.Add(_objCuView);
            }
            rptExpences.DataSource = _listCusCat;
            rptExpences.DataBind();
        }
    }
}