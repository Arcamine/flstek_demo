﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        services objser = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                txtUserFirstName.Text = objser.get_Firstname(Convert.ToInt32(Session["UserID"].ToString()));
                lbToday.Text = DateTime.Now.ToString("dd MMM yyyy");
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }
    }
}