﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class Incomes1 : System.Web.UI.Page
    {
        services _objSer = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                if (!IsPostBack)
                {
                    List<IncomeCategory> listExCat = _objSer.get_InCategory(Session["UserID"].ToString());
                    ddlCat.DataSource = listExCat;
                    ddlCat.DataBind();
                }
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string[] arr_date = txtDate.Value.Split('/');
            string date_new = arr_date[1] + "-" + arr_date[0] + "-" + arr_date[2];
            Incomes _objIncome = new Incomes()
            {
                CatID = Convert.ToInt32(ddlCat.SelectedItem.Value),
                UserID = Convert.ToInt32(Session["UserID"].ToString()),
                Amount = Convert.ToDecimal(txtInputAmt.Text),
                Date = Convert.ToDateTime(date_new)
            };

            string _result = _objSer.CreateIncome(_objIncome);
            Response.Write("<script>alert('" + _result + "')</script>");
            txtInputAmt.Text = "";
            txtDate.Value = "";
        }
    }
}