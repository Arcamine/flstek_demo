﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebServices.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style>
        .modalDialog {
	            position: fixed;
	            font-family: Arial, Helvetica, sans-serif;
	            top: 0;
	            right: 0;
	            bottom: 0;
	            left: 0;
	            background: rgba(0,0,0,0.8);
	            z-index: 99999;
	            opacity:0;
	            -webkit-transition: opacity 400ms ease-in;
	            -moz-transition: opacity 400ms ease-in;
	            transition: opacity 400ms ease-in;
	            pointer-events: none;
            }
        .modalDialog:target {
	            opacity:1;
	            pointer-events: auto;
            }

            .modalDialog > div {
	            width: 400px;
	            position: relative;
	            margin: 5% auto;
	            padding: 5px 20px 13px 20px;
	            border-radius: 10px;
	            background: #fff;
	            background: -moz-linear-gradient(#fff, #999);
	            background: -webkit-linear-gradient(#fff, #999);
	            background: -o-linear-gradient(#fff, #999);
            }
            .close {
	            background: #606061;
	            color: #FFFFFF;
	            line-height: 25px;
	            position: absolute;
	            right: -12px;
	            text-align: center;
	            top: -10px;
	            width: 24px;
	            text-decoration: none;
	            font-weight: bold;
	            -webkit-border-radius: 12px;
	            -moz-border-radius: 12px;
	            border-radius: 12px;
	            -moz-box-shadow: 1px 1px 3px #000;
	            -webkit-box-shadow: 1px 1px 3px #000;
	            box-shadow: 1px 1px 3px #000;
            }

            .close:hover { background: #00d9ff; }

            .txtStyle {
              width: 100%;
              padding: 12px 20px;
              margin: 8px 0;
              display: inline-block;
              border: 1px solid #ccc;
              border-radius: 4px;
              box-sizing: border-box;
            }

            .txtStyle1{
                    padding: 8px 16px;
                    margin: 8px 0px 0px 20px;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    box-sizing: border-box;
                    display: inline-block;
            }

            .button {
              background-color: #4CAF50; /* Green */
              border: none;
              color: white;
              padding: 8px 33px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 16px;
              margin: 4px 2px;
              cursor: pointer;
              -webkit-transition-duration: 0.4s; /* Safari */
              transition-duration: 0.4s;
            }

            .button:hover {
              box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            }

            .fntStyle{
                font-family:Arial, Helvetica, sans-serif;
            }

            .button1 {
              background-color: #4CAF50; /* Green */
              border: none;
              color: white;
              padding: 7px 7px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 16px;
              margin: 4px 2px;
              transition-duration: 0.4s;
              cursor: pointer;
              font-family:Arial, Helvetica, sans-serif;
            }

            .button1 {
              background-color: white; 
              color: black; 
              border: 2px solid #4CAF50;
            }

            .button1:hover {
              background-color: #4CAF50;
              color: white;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin: 7% 25% 0% 25%; border-inline-color: inherit; border: solid #439091; background-color: #c5fdf7;">
            <div style="width: 100%;text-align: center;margin-top: 6%;">
                <div style="width: 50%;text-align: justify;display: inline;">
                    <span class="fntStyle">Username</span>
                </div>
                <div style="width:50%;display: inline;">
                    <asp:TextBox ClientIDMode="Static" ID="txtUserName" runat="server" CssClass="txtStyle1"></asp:TextBox>
                </div>
            </div>
            <div style="width: 100%;text-align: center;margin-top: 18px;">
                <div style="width: 50%;display: inline;">
                    <span class="fntStyle">Password</span>
                </div>
                <div style="width: 50%;display: inline;">
                    <asp:TextBox ClientIDMode="Static" ID="txtPassword" runat="server" TextMode="Password"  CssClass="txtStyle1"></asp:TextBox>
                </div>
            </div>
            <div style="width: 100%;text-align: center;margin-top: 18px;">
                <div style="display: inline;">
                    <asp:Button ClientIDMode="Static" ID="btnLogin" Text="Login" OnClick="btnLogin_Click" runat="server" CssClass="button" />
                </div>
            </div>
            <div style="width: 100%;text-align: center;margin-top: 8px;margin-bottom: 6%;">
                <div style="display: inline;">
                    <a href="#openModal" class="button1">Create User</a>
                </div>
            </div>
            
        </div>
        <div id="openModal" class="modalDialog">
	            <div>
		            <a href="#close" title="Close" class="close">X</a>
		            <h2>User Registration</h2>
                        <div style="width: 100%;text-align: left;margin-top: 6%;">
                            <div style="display: inline;">
                                <asp:TextBox ClientIDMode="Static" ID="TextBox1" runat="server" placeholder="Enter First Name" CssClass="txtStyle"></asp:TextBox>
                            </div>
                        </div>
                        <div style="width: 100%;text-align: left;margin-top: 18px;">
                            <div style="display: inline;">
                                <asp:TextBox ClientIDMode="Static" ID="TextBox2" runat="server" placeholder="Enter Last Name" CssClass="txtStyle"></asp:TextBox>
                            </div>
                        </div>
                        <div style="width: 100%;text-align: left;margin-top: 6%;">
                            <div style="display: inline;">
                                <asp:TextBox ClientIDMode="Static" ID="TextBox3" runat="server" placeholder="Enter Email ID" CssClass="txtStyle" TextMode="Email"></asp:TextBox>
                            </div>
                        </div>
                        <div style="width: 100%;text-align: left;margin-top: 18px;">
                            <div style="display: inline;">
                                <asp:TextBox ClientIDMode="Static" ID="TextBox4" runat="server" placeholder="Enter User Name" CssClass="txtStyle"></asp:TextBox>
                            </div>
                        </div>
                        <div style="width: 100%;text-align: left;margin-top: 6%;">
                            <div style="display: inline;">
                                <asp:TextBox ClientIDMode="Static" ID="TextBox5" runat="server" placeholder="Enter Password" CssClass="txtStyle"></asp:TextBox>
                            </div>
                        </div>
                        <div style="width: 100%;text-align: left;margin-top: 18px;">
                            <div style="display: inline;">
                                <asp:Button ClientIDMode="Static" ID="CreateUser" Text="Create" OnClick="CreateUser_Click" runat="server" CssClass="button" />
                            </div>
                        </div>
	            </div>
        </div>
    </form>
</body>
</html>
