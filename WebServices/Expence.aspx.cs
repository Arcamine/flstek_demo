﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebServices.localhost;

namespace WebServices
{
    public partial class Expence : System.Web.UI.Page
    {

        services objser = new services();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                List<ExpenseCategory> listExCat = objser.get_ExCategory(Session["UserID"].ToString());
                ddlCat.DataSource = listExCat;
                ddlCat.DataTextField = "Name";
                ddlCat.DataValueField = "ID";
                ddlCat.DataBind();
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }
    }
}