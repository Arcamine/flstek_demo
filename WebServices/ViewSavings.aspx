﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="ViewSavings.aspx.cs" Inherits="WebServices.ViewSavings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-panel">        
            <div class="content-wrapper">
                <div class="row">
                     <div class="form-group" style="margin-left:26px">
                              <label for="exampleDateSearch">Date From</label>
                                <div id="datepicker-popup" class="input-group date datepicker">
                                    <input type="text" class="form-control" runat="server" id="txtDateFrom" autocomplete="off">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="ti-calendar input-group-text"></span>
                                        </span>
                                </div>
                            </div>

                    <div class="form-group" style="margin-left:20px">
                              <label for="exampleDateSearch">Date To</label>
                                <div id="datepicker-popup1" class="input-group date datepicker">
                                    <input type="text" class="form-control" runat="server" id="txtDateTo" autocomplete="off">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="ti-calendar input-group-text"></span>
                                        </span>
                                </div>
                            </div>

                    <asp:Button runat="server" CssClass="btn btn-primary" Text="Search" ID="btnSubmit" style="padding: 14px !important;height: 48px;margin-top: 30px;margin-left: 20px;" OnClick="btnSubmit_Click1" />
                    <div class="col-lg-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Savings View</h4>
                          <div class="table-responsive pt-3">
                              <asp:Repeater ID="rptSavings" runat="server">
                                  <HeaderTemplate>
                                      <table class="table table-dark">
                                      <thead>
                                        <tr>
                                          <th>
                                            #
                                          </th>
                                          <th>
                                            Amount
                                          </th>
                                          <th>
                                            Date
                                          </th>
                                        </tr>
                                      </thead>
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                      <tbody>
                                        <tr>
                                          <td>
                                            <asp:Label ID="lblSlNo" runat="server" Text='<%# Eval("SlNo") %>' />
                                          </td>
                                             <td>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd MMM yyyy") %>' />
                                          <td>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Convert.ToDouble(Eval("Amount")).ToString("0.00") %>' />
                                          </td>
                                         
                                          </td>
                                        </tr>
                                      </tbody>
                                  </ItemTemplate>
                                  <FooterTemplate>
                                      </table>
                                  </FooterTemplate>
                               </asp:Repeater>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
      <script>
         $(document).ready(function () {
             $('#datepicker-popup').datepicker();
             $('#datepicker-popup1').datepicker();
         });

      </script>
</asp:Content>
