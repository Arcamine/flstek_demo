﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="IncomeCategory.aspx.cs" Inherits="WebServices.IncomeCategory1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-panel">        
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Income Category</h4>
                            <div class="form-group">
                              <label for="exampleInputName1">Name</label>
                                <asp:TextBox ID="txtInputName" CssClass="form-control" runat="server" placeholder="Name" autocomplete="off"></asp:TextBox >
                            </div>
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary mr-2" Text="Submit" OnClick="btnSave_Click" />
                            <button class="btn btn-light">Cancel</button>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <div class="table-responsive pt-3">
                              <asp:Repeater ID="rptCategory" runat="server">
                                  <HeaderTemplate>
                                      <table class="table table-dark">
                                      <thead>
                                        <tr>
                                          <th>
                                            #
                                          </th>
                                          <th>
                                            Name
                                          </th>
                                          <th>
                                            Status
                                          </th>
                                        </tr>
                                      </thead>
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                      <tbody>
                                        <tr>
                                          <td>
                                            <asp:Label ID="lblSlNo" runat="server" Text='<%# Eval("SlNo") %>' />
                                          </td>
                                          <td>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                          </td>
                                          <td>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' />
                                          </td>
                                        </tr>
                                      </tbody>
                                  </ItemTemplate>
                                  <FooterTemplate>
                                      </table>
                                  </FooterTemplate>
                               </asp:Repeater>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
</asp:Content>
